from django.contrib import admin

from posts.models import Posts


@admin.register(Posts)
class PostsAdmin(admin.ModelAdmin):
    readonly_fields = ('dt_create', 'dt_update')
