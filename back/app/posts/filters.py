from django_filters import CharFilter
from django_filters import rest_framework as filters
from rest_framework.filters import SearchFilter

from posts.models import Posts


class PostsFilter(filters.FilterSet):
    title = CharFilter(field_name='title', lookup_expr='contains')
    user_name = CharFilter(field_name='user__username', lookup_expr='contains')

    class Meta:
        model = Posts
        fields = ['title', 'user_name']


class PostsSearchFilter(SearchFilter):
    title = CharFilter(field_name='title', lookup_expr='contains')
    user_name = CharFilter(field_name='user__username', lookup_expr='contains')

    class Meta:
        model = Posts
        fields = ['title', 'user_name']
