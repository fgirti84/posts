import uuid

from django.contrib.auth.models import User
from django.db import models


class Posts(models.Model):
    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        editable=False
    )

    title = models.CharField(
        max_length=100,
        verbose_name='Заголовок'
    )

    description = models.TextField(
        verbose_name='Описание'
    )

    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        verbose_name='Автор'
    )

    dt_create = models.DateTimeField(
        auto_now_add=True,
        verbose_name='Дата создания',
        null=True
    )

    dt_update = models.DateTimeField(
        auto_now=True,
        verbose_name='Дата изменения',
        null=True
    )

    def __str__(self):
        return f'Title: {self.title} - User: {self.user.username} (Id: {self.id}) '

    class Meta:
        verbose_name = 'Пост'
        verbose_name_plural = "Посты"
