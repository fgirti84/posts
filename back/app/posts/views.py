from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.viewsets import ModelViewSet

from posts.filters import PostsFilter
from posts.models import Posts
from posts.serializers import PostsSerializer


class PostsViewSet(ModelViewSet):
    queryset = Posts.objects.all()
    serializer_class = PostsSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_class = PostsFilter
