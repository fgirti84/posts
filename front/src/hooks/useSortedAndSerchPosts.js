import {
    ref,
    computed
} from 'vue';

export function useSortedAndSerchPosts(sortedPosts) {
    const serchQuery = ref('');
    const sortedAndSerchPosts = computed(() => {
        return sortedPosts.value.filter((post) =>
            post.title.toLowerCase().includes(serchQuery.value.toLowerCase())
        );
    });
    return {
        serchQuery,
        sortedAndSerchPosts
    }
};