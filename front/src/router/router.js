import Main from '@/pages/Main.vue'
import { createRouter, createWebHistory } from 'vue-router';
import PagePosts from '@/pages/PagePosts.vue'
import PagePostsStore from '@/pages/PagePostsStore.vue'
import PagePostsComposition from '@/pages/PagePostsComposition.vue'
import PagePost from '@/pages/PagePost.vue'
import PageAbout from '@/pages/PageAbout.vue'
const routes = [
    {
        path: '/',
        component: Main,
    },
    {
        path: '/posts',
        component: PagePosts,
    },
    {
        path: '/about',
        component: PageAbout,
    },
    {
        path: '/posts/:id',
        component: PagePost,
    },
    {
        path: '/store',
        component: PagePostsStore,
    },
    {
        path: '/composition',
        component: PagePostsComposition,
    },
];

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes,
    
});

export default router;