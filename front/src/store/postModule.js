import axios from "axios";

export const postModule = {
    state: () => ({
        posts: [],
        isPostsLoading: false,
        selectedSort: "",
        serchQuery: "",
        page: 1,
        limit: 10,
        totalPages: 0,
        sortOptions: [{
                value: "title",
                name: "По названию"
            },
            {
                value: "body",
                name: "По описанию"
            },
        ],
    }),
    getters: {
        sortedPosts(state) {
            return [...state.posts].sort((post1, post2) =>
                post1[state.selectedSort]?.localeCompare(post2[state.selectedSort])
            );
        },
        sortedAndSerchPosts(state, getters) {
            return getters.sortedPosts.filter((post) =>
                post.title.toLowerCase().includes(state.serchQuery.toLowerCase())
            );
        },
    },
    mutations: {
        setPosts(state, posts) {
            state.posts = posts;
        },
        setLoading(state, loading) {
            state.isPostsLoading = loading;
        },
        setSelectedSort(state, selectedSort) {
            state.selectedSort = selectedSort;
        },
        setSerchQuery(state, serchQuery) {
            state.serchQuery = serchQuery;
        },
        setPage(state, page) {
            state.page = page;
        },
        setLimit(state, limit) {
            state.limit = limit;
        },
        setTotalPages(state, totalPages) {
            state.totalPages = totalPages;
        },
        setSortOptions(state, sortOptions) {
            state.sortOptions = sortOptions;
        },
    },
    actions: {
        async fetchPosts({
            state,
            commit
        }) {
            try {
                commit('setLoading', true);
                const response = await axios.get(
                    "https://jsonplaceholder.typicode.com/posts", {
                        params: {
                            _page: state.page,
                            _limit: state.limit,
                        },
                    }
                );
                commit('setTotalPages', Math.ceil(
                    response.headers["x-total-count"] / state.limit
                ));
                commit('setPosts',response.data);

            } catch (e) {
                alert("Ошибка при получении постов: \n" + e);
            } finally {
                commit('setLoading', false);
            }
        },
    },
    namespaced: true,
}